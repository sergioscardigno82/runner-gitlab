FROM node:lts-bookworm-slim

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update \
    && apt install -y --no-install-recommends sudo \
    && apt autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && echo "node ALL=(ALL) NOPASSWD: ALL" >/etc/sudoers.d/node \
    && chmod 0440 /etc/sudoers.d/node

WORKDIR /usr/src/app
COPY myapp/package*.json ./
RUN npm install
COPY myapp/. .
EXPOSE 3000
CMD ["node", "server.js"]