# TP-16 runners gitlab Grupo 2
**Integrantes:** Monica Fernandez, Jorge Scarenzi, Sergio Scardigno, Esteban Muñoz, Julian Zanetti, Daniel Ballesteros

## Consignas
- [x] Punto 1: Crear repositorio Gitlab con aplicacion nodejs
- [x] Punto 2: Crear pipeline CI/CD
- [x] Punto 3: Crear runner de gitlab
- [x] Punto 4: Exponer aplicacion
- [x] Punto 5: Documentar en confluence

### Prender el cluster local
```bash
minikube start --cpus 4 --memory 8192
```

### Crear repositorio Gitlab con aplicacion nodejs
Deberan crear un repositorio de Gitlab con un proyecto de ejemplo en lenguaje nodejs. 

> Ejecutar el servicio de node.js:
> ```bash
>   node app.js
> ```

### Crear pipeline CI/CD
Deberán crear un pipeline de CI/CD con al menos 3 stages (build - test - deploy)  
La ejecución automática del pipeline deberá tener reglas (al menos 2 - ej: por commit o por merge request)  
El deploy deberán hacerlo en su cluster de minikube  

> Variables a configurar en el pipeline de gitlab. Ya estan configuradas en este repo  
> 
> ```bash
> CI_REGISTRY=docker.io
> CI_REGISTRY_IMAGE=index.docker.io/devdanka/tp-16
> CI_REGISTRY_PASSWORD=dckr_pat_YCxu-
> CI_REGISTRY_USER=devdanka
> CI_JOB_TOKEN=glpat-zaEy
> ```

### Crear runner de gitlab
Deberán crear un runner en su cluster de minikube y asociarlo a su cuenta de gitlab
https://docs.gitlab.com/runner/install/kubernetes.html

#### Agregar runner
1. Ir a configuraciones del repo, bajo la opcion CI/CD ---> Runners ---> New Project runner
![Paso 1](imagenes-docu/image-1.png)  
2. Ponerle nombre y tags de ser necesario
![Paso 2](imagenes-docu/image-2.png)  
3. Nos devuelve un token de runner, y ese token lo ingresamos en el archivo gitlab-runner/values.yml en la linea 13
![Paso 3](imagenes-docu/image-3.png)  
![Linea 13](imagenes-docu/image-7.png)
4. Desactivar shared runners
![Paso 4](imagenes-docu/image-4.png)
5. Darle los permisos a la cuenta de servicios para poder ejecutar el pipeline en el clúster    
> ```bash
> kubectl apply -f gitlab-runner/clusterrole.yml 
> ```
6. Crear el runner en el cluster
> ```bash
> helm repo add gitlab https://charts.gitlab.io
> helm repo update gitlab
> helm install --namespace default gitlab-runner-docker -f gitlab-runner/values.yml gitlab/gitlab-runner
> ```
7. Chequear que la aplicacion esta levantada
![Get service, deploy y pod](imagenes-docu/image-6.png)
8. Ver que el pipeline este completo
![Pipeline](imagenes-docu/image-8.png)

### Exponer aplicacion
La aplicacion se expone por medio de servicio LoadBalancer "mi-aplicacion-clima-service"

Chequear creacion de servicio:
kubectl get service mi-aplicacion-clima-service

![LoadBalancer](imagenes-docu/image-10.PNG)

Chequear la url de nuestra aplicacion:

minikube service mi-aplicacion-clima-service --url

validacion de URL:
curl http://IP:PortAssigned/

### Documentar
La presente es la documentacion paso a paso
